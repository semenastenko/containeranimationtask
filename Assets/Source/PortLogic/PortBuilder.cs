﻿using System.ComponentModel;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace ContainerAnimationTask.Port
{
    public class PortBuilder : IInitializable
    {
        private readonly PortSettings _settings;

        private readonly Container.Factory _containerFactory;

        private readonly InspectionController _inspectionController;

        private PortArea _portArea;

        public PortBuilder(PortSettings settings, Container.Factory containerFactory, InspectionController inspectionController)
        {
            _settings = settings;
            _containerFactory = containerFactory;
            _inspectionController = inspectionController;
        }

        public void Initialize()
        {
            BuildPortArea();

        }

        private void BuildPortArea()
        {
            var objectTransform = new GameObject("Port Area").transform;
            _portArea = new PortArea(objectTransform);
            for (int x = 0; x < _settings.PortAreaSize.x; x++)
            {
                for (int y = 0; y < _settings.PortAreaSize.y; y++)
                {
                    CreatePlatformArea(new Vector2Int(x, y), _portArea);
                }
            }
        }

        private void CreatePlatformArea(Vector2Int position, PortArea portArea)
        {
            var objectTrnsform = new GameObject($"Platform ({position})").transform;
            objectTrnsform.SetParent(portArea.ParentTransform);
            objectTrnsform.position = GetPlatformAreaPosition(position);

            var platform = new PlatformArea(position, objectTrnsform);
            portArea.Add(platform);

            BuildContainerAreas(platform);
        }

        private Vector3 GetPlatformAreaPosition(Vector2Int position) => new Vector3(
                position.x * (_settings.PlatformAreaSize.x * (_settings.ContainerAreaSize.x + _settings.ContainerSpacing.x) + _settings.PlatformSpacing.x), 0,
                position.y * (_settings.PlatformAreaSize.z * (_settings.ContainerAreaSize.z + _settings.ContainerSpacing.z) + _settings.PlatformSpacing.y));

        private void BuildContainerAreas(PlatformArea platformArea)
        {
            for (int x = 0; x < _settings.PlatformAreaSize.x; x++)
            {
                for (int y = 0; y < _settings.PlatformAreaSize.y; y++)
                {
                    for (int z = 0; z < _settings.PlatformAreaSize.z; z++)
                    {
                        CreateContainerArea(new Vector3Int(x, y, z), platformArea);
                    }
                }
            }
        }

        private void CreateContainerArea(Vector3Int position, PlatformArea platformArea)
        {
            var container = _containerFactory.Create();

            if (IsCorrectSize(container.Size))
            {
                container.transform.SetParent(platformArea.ParentTransform);
                var localPosition = GetContainerAreaPosition(position);

                var command = new Command(() => _inspectionController.InspectSection(platformArea, container.Info.Section));

                container.SetParameters(position, localPosition + platformArea.ParentTransform.position, command);
                var containerArea = new ContainerArea(container);
                platformArea.Add(containerArea);
            }
            else
            {
                throw new IncorrectContainerSizeExeption();
            }
        }

        private Vector3 GetContainerAreaPosition(Vector3Int position) => new Vector3(
                    _settings.ContainerAreaSize.x / 2 + position.x * (_settings.ContainerAreaSize.x + _settings.ContainerSpacing.x),
                    _settings.ContainerAreaSize.y / 2 + position.y * (_settings.ContainerAreaSize.y + _settings.ContainerSpacing.y),
                    _settings.ContainerAreaSize.z / 2 + position.z * (_settings.ContainerAreaSize.z + _settings.ContainerSpacing.z));

        private bool IsCorrectSize(Vector3 size) =>
            size.x <= _settings.ContainerAreaSize.x &&
            size.y <= _settings.ContainerAreaSize.y &&
            size.z <= _settings.ContainerAreaSize.z;
    }
}
