﻿using UnityEngine;

namespace ContainerAnimationTask.Port
{
    [CreateAssetMenu(fileName = "PortSettings", menuName = "SO/PortSettings")]
    public class PortSettings : ScriptableObject
    {
        [Header("Port")]
        [SerializeField]
        private Vector2Int portAreaSize;

        [SerializeField]
        private Vector2 platformSpacing;


        [Header("Platform")]
        [SerializeField]
        private Vector3Int platformAreaSize;

        [SerializeField]
        private Vector3 containerSpacing;


        [Header("Container")]
        [SerializeField]
        private Vector3 containerAreaSize;

        public Vector2Int PortAreaSize => portAreaSize;
        public Vector2 PlatformSpacing => platformSpacing;
        public Vector3Int PlatformAreaSize => platformAreaSize;
        public Vector3 ContainerSpacing => containerSpacing;
        public Vector3 ContainerAreaSize => containerAreaSize;
    }
}
