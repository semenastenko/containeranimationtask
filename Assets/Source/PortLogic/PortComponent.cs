using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ContainerAnimationTask.Port
{
    public abstract class PortComponent
    {
        public virtual bool IsComposite => false;

        public void Add(PortComponent component)
        {
            if (IsComposite)
            {
                OnAdded(component);
            }
            else
            {
                throw new NotCompositeExeption();
            }
        }

        public void Remove(PortComponent component)
        {
            if (IsComposite)
            {
                OnRemoved(component);
            }
            else
            {
                throw new NotCompositeExeption();
            }
        }

        protected virtual void OnAdded(PortComponent component)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnRemoved(PortComponent component)
        {
            throw new NotImplementedException();
        }

        public bool TryAdd(PortComponent component)
        {
            try
            {
                Add(component);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool TryRemove(PortComponent component)
        {
            try
            {
                Remove(component);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
