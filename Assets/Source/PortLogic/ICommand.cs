﻿namespace ContainerAnimationTask
{
    public interface ICommand
    {
        void Execute();
    }
}
