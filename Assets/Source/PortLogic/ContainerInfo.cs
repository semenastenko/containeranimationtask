﻿using System;

namespace ContainerAnimationTask.Port
{
    public struct ContainerInfo
    {
        public readonly Guid GUID;

        public readonly int Section;

        public readonly int Row;

        public readonly int Level;

        public ContainerInfo(Guid guid, int z, int x, int y)
        {
            GUID = guid;
            Section = z;
            Row = x;
            Level = y;
        }
    }
}