﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ContainerAnimationTask.Port
{
    public class PortArea : PortComponent
    {
        protected List<PortComponent> _children;

        private Transform _parentTransform;

        public override bool IsComposite => true;

        public IReadOnlyList<PortComponent> Platforms => _children;

        public Transform ParentTransform => _parentTransform;

        public PortArea(Transform parentTransform)
        {
            _children = new();
            _parentTransform = parentTransform;
        }

        protected override void OnAdded(PortComponent component)
        {
            if (component is PlatformArea)
            {
                _children.Add(component);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        protected override void OnRemoved(PortComponent component)
        {
            if (component is PlatformArea)
            {
                _children.Remove(component);
            }
            else
            {
                throw new ArgumentException();
            }
            
        }
    }
}
