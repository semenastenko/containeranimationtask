﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

namespace ContainerAnimationTask.Port
{
    public class PlatformArea : PortComponent
    {
        protected List<PortComponent> _children;

        private Vector2Int _position;

        private Transform _parentTransform;

        public override bool IsComposite => true;

        public IReadOnlyList<ContainerArea> Containers => _children.Select(x => x as ContainerArea).ToList();

        public Vector2Int Position => _position;

        public Transform ParentTransform => _parentTransform;

        public PlatformArea(Vector2Int position, Transform parentTransform)
        {
            _children = new();
            _position = position;
            _parentTransform = parentTransform;
        }

        protected override void OnAdded(PortComponent component)
        {
            if (component is ContainerArea)
            {
                _children.Add(component);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        protected override void OnRemoved(PortComponent component)
        {
            if (component is ContainerArea)
            {
                _children.Remove(component);
            }
            else
            {
                throw new ArgumentException();
            }

        }
    }
}
