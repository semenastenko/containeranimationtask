﻿using UnityEngine;

namespace ContainerAnimationTask.Port
{
    public class ContainerArea : PortComponent
    {
        private Container _container;

        public ContainerInfo Info => _container.Info;

        public Vector3 WorldPosition 
        {
            get => _container.transform.position;
            set => _container.transform.position = value;
        }
        public Vector3 StartPosition => _container.StartPosition;

        public ContainerArea(Container container)
        {
            _container = container;
        }
    }
}
