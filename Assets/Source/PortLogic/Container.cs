using ContainerAnimationTask.Interactions;
using ModestTree;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace ContainerAnimationTask.Port
{
    public class Container : MonoBehaviour, ISelectable
    {
        ICommand _selectCommand;

        private ContainerInfo _info;

        private Vector3 _startPosition;

        public Vector3 Size => transform.localScale;
        
        public ContainerInfo Info => _info;

        public Vector3 StartPosition => _startPosition;

        public void SetParameters(Vector3Int platformPosition, Vector3 worldPosition, ICommand selectCommand)
        {
            _startPosition = worldPosition;
            _selectCommand = selectCommand;
            transform.position = worldPosition;
            name = $"Container ({platformPosition})";
            _info = new ContainerInfo(Guid.NewGuid(), platformPosition.z, platformPosition.x, platformPosition.y);
        }

        public void Select()
        {
            _selectCommand?.Execute();
        }

        public class Factory : PlaceholderFactory<Container>
        {
            public void Recreate()
            {

            }
        }
    }
}
