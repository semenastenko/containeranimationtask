using ContainerAnimationTask.Animation;
using ContainerAnimationTask.Enviroments;
using ContainerAnimationTask.Interactions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ContainerAnimationTask.Port
{
    public class InspectionController
    {
        private readonly AppSettings _settings;

        private Dictionary<PlatformArea, int> _inspectedSections;

        private Dictionary<PlatformArea, ContainerInspectAnimation> _containerAnimations;

        public InspectionController(AppSettings settings)
        {
            _settings = settings;
            _inspectedSections = new();
            _containerAnimations = new();
        }

        public void InspectSection(PlatformArea platform, int section)
        {
            if (_inspectedSections.ContainsKey(platform))
            {
                MoveDown(platform);
                if (_inspectedSections[platform] == section)
                {
                    _inspectedSections.Remove(platform);
                }
                else
                {
                    _inspectedSections[platform] = section;
                    MoveUp(platform, section);
                }
            }
            else
            {
                _inspectedSections.Add(platform, section);
                MoveUp(platform, section);
            }
        }

        private void MoveUp(PlatformArea platform, int section)
        {
            var animation = new ContainerInspectAnimation(platform, section, _settings.AnimationTime, _settings.AnimationDistance);
            _containerAnimations.Add(platform, animation);
            animation.MoveUp();
        }

        private void MoveDown(PlatformArea platform)
        {
            var animation = _containerAnimations[platform];
            _containerAnimations.Remove(platform);
            animation.MoveDown();
        }
    }
}
