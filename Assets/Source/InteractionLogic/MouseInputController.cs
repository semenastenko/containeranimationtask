﻿using System;
using UniRx;
using UnityEngine;

namespace ContainerAnimationTask.Interactions
{
    public class MouseInputController : IDisposable
    {
        private IDisposable _updateDispose;

        private Vector3 lastMousePosition;

        public bool IsLocked => Cursor.lockState == CursorLockMode.Locked;

        public event Action<Vector3> OnMove;

        public event Action<Vector3> OnRightClick;

        public event Action<Vector3> OnLeftClick;

        public MouseInputController()
        {
            _updateDispose = Observable.EveryUpdate().Subscribe(_ => Update());
        }
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnLeftClick?.Invoke(Input.mousePosition);
            }
            if (Input.GetMouseButtonDown(1))
            {
                OnRightClick?.Invoke(Input.mousePosition);
            }
            if (lastMousePosition != Input.mousePosition)
            {
                OnMove?.Invoke(Input.mousePosition);
                lastMousePosition = Input.mousePosition;
            }
        }


        public void Dispose()
        {
            _updateDispose.Dispose();
            OnMove = null;
            OnRightClick = null;
            OnLeftClick = null;
        }
    }
}
