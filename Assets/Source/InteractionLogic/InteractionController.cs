using ContainerAnimationTask.Enviroments;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

namespace ContainerAnimationTask.Interactions
{
    public class InteractionController : IDisposable
    {
        private readonly MouseInputController _mouseInput;

        private readonly Camera _camera;

        private readonly AppSettings _settings;

        private IHoverable currentHover;

        public event Action<IHoverable> OnHover;

        public event Action<IHoverable> OnHoverEnter;

        public event Action<IHoverable> OnHoverExit;

        public event Action<ISelectable> OnSelect;

        public InteractionController(MouseInputController mouseInput, Camera camera, AppSettings settings)
        {
            _mouseInput = mouseInput;
            _camera = camera;
            _settings = settings;

            mouseInput.OnLeftClick += OnMouseLeftClick;
            mouseInput.OnMove += OnMouseMove;
        }

        private void OnMouseLeftClick(Vector3 position)
        {
            var hit = Raycast(position);
            if (hit.HasValue)
            {
                SelectHandler(hit.Value);
            }
        }

        private void OnMouseMove(Vector3 position)
        {
            var hit = Raycast(position);
            if (hit.HasValue)
            {
                HoverHandler(hit.Value);
            }
            else
            {
                CheckHover(null);
            }
        }

        private RaycastHit? Raycast(Vector3 position)
        {
            Ray ray = _camera.ScreenPointToRay(position);
            RaycastHit hit;
            if (!_mouseInput.IsLocked && Physics.Raycast(ray, out hit, _settings.MaxInteractionDistance))
                return hit;
            return null;
        }

        private void SelectHandler(RaycastHit hit)
        {
            var component = hit.collider.GetComponent<ISelectable>();
            if (component != null)
            {
                component.Select();
                OnSelect?.Invoke(component);
            }
        }

        private void HoverHandler(RaycastHit hit)
        {
            var component = hit.collider.GetComponent<IHoverable>();
            CheckHover(component);
        }

        private void CheckHover(IHoverable hover)
        {
            if (currentHover != hover)
            {
                if (currentHover != null)
                    OnHoverExit?.Invoke(currentHover);
                if (hover != null)
                    OnHoverEnter?.Invoke(hover);
                currentHover = hover;
            }

            if (currentHover != null)
                OnHover?.Invoke(hover);
        }

        public void Dispose()
        {
            _mouseInput.OnLeftClick -= OnMouseLeftClick;
            _mouseInput.OnMove -= OnMouseMove;
        }
    }
}
