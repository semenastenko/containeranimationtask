﻿namespace ContainerAnimationTask.Interactions
{
    public interface ISelectable : IHoverable
    {
        void Select();
    }
}
