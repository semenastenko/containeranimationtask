﻿using UnityEngine;

namespace AnimationTool
{
    public abstract class Easing
    {
        protected const float BACK_VALUE_IN_OR_OUT = 1.70158f;
        protected const float BACK_VALUE_IN_AND_OUT = 2.5949095f;

        public static Easing Linear => new LinearEase();
        public static Easing QuadIn => new QuadraticInEase();
        public static Easing QuadOut => new QuadraticOutEase();
        public static Easing QuadInOut => new QuadraticInOutEase();
        public static Easing CubIn => new CubicInEase();
        public static Easing CubOut => new CubicOutEase();
        public static Easing CubInOut => new CubicInOutEase();
        public static Easing QuartIn => new QuarticInEase();
        public static Easing QuartOut => new QuarticOutEase();
        public static Easing QuartInOut => new QuarticInOutEase();
        public static Easing SinIn => new SinusoidalInEase();
        public static Easing SinOut => new SinusoidalOutEase();
        public static Easing SinInOut => new SinusoidalInOutEase();
        public static Easing ExpIn => new ExponentialInEase();
        public static Easing ExpOut => new ExponentialOutEase();
        public static Easing ExpInOut => new ExponentialInOutEase();
        public static Easing CircIn => new CircularInEase();
        public static Easing CircOut => new CircularOutEase();
        public static Easing CircInOut => new CircularInOutEase();
        public static Easing ElasticIn => new ElasticInEase();
        public static Easing ElasticOut => new ElasticOutEase();
        public static Easing ElasticInOut => new ElasticInOutEase();
        public static Easing BackIn => new BackInEase();
        public static Easing BackOut => new BackOutEase();
        public static Easing BackInOut => new BackInOutEase();
        public static Easing BounceIn => new BounceInEase();
        public static Easing BounceOut => new BounceOutEase();
        public static Easing BounceInOut => new BounceInOutEase();

        public abstract float Ease(float k);

        protected class LinearEase : Easing
        {
            public override float Ease(float k)
            {
                return k;
            }
        }
        protected class QuadraticInEase : Easing
        {
            public override float Ease(float k)
            {
                return k * k;
            }
        };
        protected class QuadraticOutEase : Easing
        {
            public override float Ease(float k)
            {
                return k * (2f - k);
            }
        };
        protected class QuadraticInOutEase : Easing
        {

            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return 0.5f * k * k;
                return -0.5f * ((k -= 1f) * (k - 2f) - 1f);
            }
        };
        protected class CubicInEase : Easing
        {
            public override float Ease(float k)
            {
                return k * k * k;
            }
        };
        protected class CubicOutEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f + (k -= 1f) * k * k;
            }
        };
        protected class CubicInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return 0.5f * k * k * k;
                return 0.5f * ((k -= 2f) * k * k + 2f);
            }
        };
        protected class QuarticInEase : Easing
        {
            public override float Ease(float k)
            {
                return k * k * k * k;
            }
        };
        protected class QuarticOutEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f - (k -= 1f) * k * k * k;
            }
        };
        protected class QuarticInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return 0.5f * k * k * k * k;
                return -0.5f * ((k -= 2f) * k * k * k - 2f);
            }
        };
        protected class QuinticInEase : Easing
        {
            public override float Ease(float k)
            {
                return k * k * k * k * k;
            }
        };
        protected class QuinticOutEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f + (k -= 1f) * k * k * k * k;
            }
        };
        protected class QuinticInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return 0.5f * k * k * k * k * k;
                return 0.5f * ((k -= 2f) * k * k * k * k + 2f);
            }
        };
        protected class SinusoidalInEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f - Mathf.Cos(k * Mathf.PI / 2f);
            }
        };
        protected class SinusoidalOutEase : Easing
        {
            public override float Ease(float k)
            {
                return Mathf.Sin(k * Mathf.PI / 2f);
            }
        };
        protected class SinusoidalInOutEase : Easing
        {
            public override float Ease(float k)
            {
                return 0.5f * (1f - Mathf.Cos(Mathf.PI * k));
            }
        };
        protected class ExponentialInEase : Easing
        {
            public override float Ease(float k)
            {
                return k == 0f ? 0f : Mathf.Pow(1024f, k - 1f);
            }
        };
        protected class ExponentialOutEase : Easing
        {
            public override float Ease(float k)
            {
                return k == 1f ? 1f : 1f - Mathf.Pow(2f, -10f * k);
            }
        };
        protected class ExponentialInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if (k == 0f) return 0f;
                if (k == 1f) return 1f;
                if ((k *= 2f) < 1f) return 0.5f * Mathf.Pow(1024f, k - 1f);
                return 0.5f * (-Mathf.Pow(2f, -10f * (k - 1f)) + 2f);
            }
        };
        protected class CircularInEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f - Mathf.Sqrt(1f - k * k);
            }
        };
        protected class CircularOutEase : Easing
        {
            public override float Ease(float k)
            {
                return Mathf.Sqrt(1f - (k -= 1f) * k);
            }
        };
        protected class CircularInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return -0.5f * (Mathf.Sqrt(1f - k * k) - 1);
                return 0.5f * (Mathf.Sqrt(1f - (k -= 2f) * k) + 1f);
            }
        };
        protected class ElasticInEase : Easing
        {
            public override float Ease(float k)
            {
                if (k == 0) return 0;
                if (k == 1) return 1;
                return -Mathf.Pow(2f, 10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f);
            }
        };
        protected class ElasticOutEase : Easing
        {
            public override float Ease(float k)
            {
                if (k == 0) return 0;
                if (k == 1) return 1;
                return Mathf.Pow(2f, -10f * k) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f) + 1f;
            }
        };
        protected class ElasticInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return -0.5f * Mathf.Pow(2f, 10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f);
                return Mathf.Pow(2f, -10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f) * 0.5f + 1f;
            }
        };
        protected class BackInEase : Easing
        {
            public override float Ease(float k)
            {
                return k * k * ((BACK_VALUE_IN_OR_OUT + 1f) * k - BACK_VALUE_IN_OR_OUT);
            }
        };
        protected class BackOutEase : Easing
        {
            public override float Ease(float k)
            {
                return (k -= 1f) * k * ((BACK_VALUE_IN_OR_OUT + 1f) * k + BACK_VALUE_IN_OR_OUT) + 1f;
            }
        };
        protected class BackInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if ((k *= 2f) < 1f) return 0.5f * (k * k * ((BACK_VALUE_IN_AND_OUT + 1f) * k - BACK_VALUE_IN_AND_OUT));
                return 0.5f * ((k -= 2f) * k * ((BACK_VALUE_IN_AND_OUT + 1f) * k + BACK_VALUE_IN_AND_OUT) + 2f);
            }
        };
        protected class BounceInEase : Easing
        {
            public override float Ease(float k)
            {
                return 1f - BounceOut.Ease(1f - k);
            }
        };
        protected class BounceOutEase : Easing
        {
            public override float Ease(float k)
            {
                if (k < 1f / 2.75f)
                {
                    return 7.5625f * k * k;
                }
                else if (k < 2f / 2.75f)
                {
                    return 7.5625f * (k -= 1.5f / 2.75f) * k + 0.75f;
                }
                else if (k < 2.5f / 2.75f)
                {
                    return 7.5625f * (k -= 2.25f / 2.75f) * k + 0.9375f;
                }
                else
                {
                    return 7.5625f * (k -= 2.625f / 2.75f) * k + 0.984375f;
                }
            }
        };
        protected class BounceInOutEase : Easing
        {
            public override float Ease(float k)
            {
                if (k < 0.5f) return BounceIn.Ease(k * 2f) * 0.5f;
                return BounceOut.Ease(k * 2f - 1f) * 0.5f + 0.5f;
            }
        };
    }
}