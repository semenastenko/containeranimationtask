﻿using Cysharp.Threading.Tasks;
using System;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace AnimationTool
{
    public partial class LerpAnimation : IDisposable
    {
        private float _startValue;
        private float _endValue;
        private float _time;
        private Action<float> _onNext;
        private Easing _easing;

        public bool IsPlaying { get; private set; }

        public LerpAnimation(float startValue = 0, float endValue = 1, float time = 1, Easing easing = null, Action<float> onNext = null)
        {
            _startValue = startValue;
            _endValue = endValue;
            _time = time;
            _onNext = onNext;

            if (easing == null)
                _easing = Easing.Linear;
            else
                _easing = easing;
        }

        public async UniTask Play()
        {
            IsPlaying = true;
            (bool complete, float timeElapsed) progress = (true, 0);
            while (progress.complete && IsPlaying)
            {
                progress = Lerping(_startValue, _endValue, _time, progress.timeElapsed, _easing, _onNext);
                await UniTask.Yield();
            }
            IsPlaying = false;
        }

        public void Stop() => Dispose();

        private static (bool, float) Lerping(float startValue, float endValue, float time, float timeElapsed, Easing ease, Action<float> onNext)
        {
            bool complete = timeElapsed <= time;
            if (complete)
            {
                float lerpValue = Mathf.LerpUnclamped(startValue, endValue, ease.Ease(timeElapsed / time));
                timeElapsed += Time.deltaTime;
                onNext?.Invoke(lerpValue);
            }
            else
            {
                onNext?.Invoke(endValue);
            }
            return (complete, timeElapsed);
        }

        public void Dispose()
        {
            IsPlaying = false;
        }
    }
}