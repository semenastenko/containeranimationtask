﻿using AnimationTool;
using ContainerAnimationTask.Port;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;

namespace ContainerAnimationTask.Animation
{
    public class ContainerInspectAnimation
    {
        private readonly IEnumerable<ContainerArea> _containers;
        private readonly float _time;
        private readonly float _height;
        private LerpAnimation upAnimation;

        public ContainerInspectAnimation(PlatformArea platform, int section, float time, float height)
        {
            _containers = platform.Containers.Where(x => x.Info.Section.Equals(section));
            _time = time;
            _height = height;
        }
        public async void MoveUp()
        {
            var pathPositions = new Dictionary<ContainerArea, Vector3>(
                _containers.Select(x => new KeyValuePair<ContainerArea, Vector3>(x, x.WorldPosition)));

            upAnimation = new LerpAnimation(0, 1, _time, Easing.CubOut, value =>
            {
                foreach (var container in pathPositions)
                {
                    var endHeight = container.Key.StartPosition.y + _height * container.Key.Info.Level;
                    var deltaPath = value * (endHeight - container.Value.y);
                    container.Key.WorldPosition = container.Value + new Vector3(0, deltaPath, 0);
                }
            });
            await upAnimation.Play();
        }

        public async void MoveDown()
        {
            upAnimation.Stop();
            var pathPositions = new Dictionary<ContainerArea, Vector3>(
                _containers.Select(x => new KeyValuePair<ContainerArea, Vector3>(x, x.WorldPosition)));

            var downAnimation = new LerpAnimation(0, 1, _time, Easing.CubIn, value =>
            {
                foreach (var container in pathPositions)
                {
                    var deltaPath = value * (container.Key.StartPosition.y - container.Value.y);
                    container.Key.WorldPosition = container.Value + new Vector3(0, deltaPath, 0);
                }
            });
            await downAnimation.Play();
        }
    }
}