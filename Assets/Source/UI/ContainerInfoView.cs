using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace ContainerAnimationTask.UI
{
    public class ContainerInfoView : MonoBehaviour
    {
        private const int CURSOR_SIZE = 32;

        [SerializeField]
        private TMP_Text sectionText;

        [SerializeField]
        private TMP_Text rowText;

        [SerializeField]
        private TMP_Text levelText;

        [SerializeField]
        private TMP_Text guidText;

        public string Section
        {
            get => sectionText.text;
            set => sectionText.text = value;
        }
        public string Row
        {
            get => rowText.text;
            set => rowText.text = value;
        }
        public string Level
        {
            get => levelText.text;
            set => levelText.text = value;
        }
        public string GUID
        {
            get => guidText.text;
            set => guidText.text = value;
        }

        public bool IsEnable
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        public void SetTargetPosition(Vector3 position)
        {
            var upscaleValues = GetUpscaleValues();
            var size = (transform as RectTransform).rect.size;
            size = new Vector2(size.x * upscaleValues.x, size.y * upscaleValues.y);

            var xPosition = position.x + size.x / 2 + CURSOR_SIZE * upscaleValues.x;
            if (xPosition + size.x / 2 > Screen.width)
                xPosition = position.x - size.x / 2;

            var yPosition = position.y - size.y / 2 - CURSOR_SIZE * upscaleValues.y;
            if (yPosition - size.y / 2 < 0)
                yPosition = position.y + size.y / 2;

            transform.position = new Vector3(xPosition, yPosition);
        }

        private Vector2 GetUpscaleValues()
            => new Vector2(Screen.width / 1920f, Screen.height / 1080f);
    }
}
