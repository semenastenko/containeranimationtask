﻿using ContainerAnimationTask.Interactions;
using ContainerAnimationTask.Port;
using System;
using UnityEngine;

namespace ContainerAnimationTask.UI
{
    public class ContainerInfoController : IDisposable
    {
        private readonly InteractionController _interaction;
        
        private readonly ContainerInfoView _infoView;

        public ContainerInfoController(InteractionController interaction, ContainerInfoView infoView)
        {
            _interaction = interaction;
            _infoView = infoView;

            _interaction.OnHover += OnHovered;
            _interaction.OnHoverEnter += OnHoverEntered;
            _interaction.OnHoverExit += OnHoverExited;
        }

        private void OnHoverExited(IHoverable obj)
        {
            _infoView.IsEnable = false;
        }

        private void OnHoverEntered(IHoverable obj)
        {
            if (obj is Container container)
            {
                _infoView.IsEnable = true;
                _infoView.Section = (container.Info.Section + 1).ToString();
                _infoView.Row = (container.Info.Row + 1).ToString();
                _infoView.Level = (container.Info.Level + 1).ToString();
                _infoView.GUID = container.Info.GUID.ToString();
            }
        }

        private void OnHovered(IHoverable obj)
        {
            _infoView.SetTargetPosition(Input.mousePosition);
        }

        public void Dispose()
        {
            _interaction.OnHover -= OnHovered;
            _interaction.OnHoverEnter -= OnHoverEntered;
            _interaction.OnHoverExit -= OnHoverExited;
        }
    }
}
