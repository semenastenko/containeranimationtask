using ContainerAnimationTask.Enviroments;
using ContainerAnimationTask.Port;
using UnityEngine;
using Zenject;

namespace ContainerAnimationTask.Installers
{
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "Installers/SOInstaller")]
    public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
    {
        [SerializeField]
        private AppSettings _appSettings;
        [SerializeField]
        private PortSettings _portSettings;

        public override void InstallBindings()
        {
            Container.BindInstances(_appSettings, _portSettings);
        }
    }
}