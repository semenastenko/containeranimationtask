using UnityEngine;
using Zenject;
using ContainerAnimationTask.Port;
using ContainerAnimationTask.Interactions;
using ContainerAnimationTask.UI;

namespace ContainerAnimationTask.Installers
{
    public class AppInstaller : MonoInstaller
    {
        [SerializeField]
        private Container _containerPrefab;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<PortBuilder>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MouseInputController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ContainerInfoController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<InteractionController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<InspectionController>().AsSingle().NonLazy();
            Container.BindFactory<Container, Container.Factory>().FromComponentInNewPrefab(_containerPrefab);
        }
    }
}