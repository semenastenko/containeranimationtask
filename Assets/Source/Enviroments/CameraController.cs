using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace ContainerAnimationTask.Enviroments
{
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [Inject]
        private AppSettings _appSettings;

        private Vector2 currentRotation;

        private void Update()
        {
            MovementUpdate();
            RotationUpdate();
        }

        private void MovementUpdate()
        {
            var speed = Input.GetButton("Shift") ? _appSettings.CameraShiftSpeed : _appSettings.CameraSpeed;
            transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime);
            transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime);
        }

        private void RotationUpdate()
        {
            if (Input.GetMouseButton(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
                currentRotation.x += Input.GetAxis("Mouse X") * _appSettings.MouseSensetivity * Time.deltaTime;
                currentRotation.y -= Input.GetAxis("Mouse Y") * _appSettings.MouseSensetivity * Time.deltaTime;
                currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
                currentRotation.y = Mathf.Clamp(currentRotation.y, -_appSettings.MaxCameraYAngle, _appSettings.MaxCameraYAngle);
                transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);
            }
            else if (Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}