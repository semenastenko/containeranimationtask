﻿using UnityEngine;

namespace ContainerAnimationTask.Enviroments
{
    [CreateAssetMenu(fileName = "AppSettings", menuName = "SO/AppSettings")]
    public class AppSettings : ScriptableObject
    {
        [Header("Camera Settings")]
        [SerializeField]
        private float _cameraSpeed = 10f;
        [SerializeField]
        private float _cameraShiftSpeed = 50f;
        [SerializeField]
        private float _mouseSensetivity = 500f;
        [SerializeField]
        private float _maxCameraYAngle = 80f;
        [Header("Input Settings")]
        [SerializeField]
        private float _maxInteractionDistance = 100f;
        [Header("Animation Settings")]
        [SerializeField]
        private float _animationTime = 1f;
        [SerializeField]
        private float _animationDistance = 10f;

        public float CameraSpeed => _cameraSpeed;
        public float CameraShiftSpeed => _cameraShiftSpeed;
        public float MouseSensetivity => _mouseSensetivity;
        public float MaxCameraYAngle => _maxCameraYAngle;
        public float MaxInteractionDistance => _maxInteractionDistance;
        public float AnimationTime => _animationTime;
        public float AnimationDistance => _animationDistance;
    }
}