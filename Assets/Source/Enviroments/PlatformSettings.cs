﻿using UnityEngine;

namespace ContainerAnimationTask.Enviroments
{
    [CreateAssetMenu(fileName = "PlatformSettings", menuName = "SO/PlatformSettings")]
    public class PlatformSettings : ScriptableObject
    {
        [SerializeField]
        private Vector3Int _size;
        [SerializeField]
        private Vector3 _containerOffset;
        [SerializeField]
        private int _containerCount;

        public Vector3Int Size => _size;
        public Vector3 ContainerOffset => _containerOffset;
        public int ContainerCount => _containerCount;
    }
}