﻿using UnityEngine;

namespace ContainerAnimationTask.Enviroments
{
    [CreateAssetMenu(fileName = "PortAreaSettings", menuName = "SO/PortAreaSettings")]
    public class PortAreaSettings : ScriptableObject
    {
        [SerializeField]
        private Vector2Int _areaSize;
        [SerializeField]
        private Vector2 _platformOffset;

        public Vector2Int AreaSize => _areaSize;
        public Vector2 PlatformOffset => _platformOffset;
    }
}